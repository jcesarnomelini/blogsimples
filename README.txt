1 - Crie uma pasta qualquer em um lugar de sua preferência;
2 - Crie uma pasta chamada wordpress;
3 - Crie outra pasta chamada db;
4 - Copie o arquivo chamado docker-compose.yml para esta pasta;
5 - Abra o cmd ou o terminal na pasta que você criou no passo 1;
6 - Execute docker-compose up;
7 - Copie a pasta blogsimples para a pasta wordpress/wp-content/themes;
8 - Acesse localhost:8080;
9 - Selecione a linguagem de preferência;
10 - Coloque as informações necessárias e clique em instalar o WordPress;
11 - Acesse com o usuário e senha que você criou no passo anterior;
12 - Vá em Aparência - Temas e ative o tema blogsimples;
13 - Volte ao localhost:8080.